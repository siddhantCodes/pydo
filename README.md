# Pydo

A simple todo app made using python and the flask framework.

## Running

Configure an environment variable `DB_URI` with a postgres connection uri. Use
the `db-dev.sh` script in the `scripts` folder to start a preconfigured docker
image.

```sh

# clone repo
git clone https://gitlab.com/siddhantCodes/pydo
cd pydo

# create environment
python -m venv venv
# activate the virtual environment
source venv/bin/activate

# install dependencies
pip install -r requirements.txt

# run
python app.py
```

The server, by default, listens on port 3000. Open `localhost:3000` to open
this app.
