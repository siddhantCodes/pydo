#!/bin/sh

docker run --name pydo -d \
  -p 5432:5432 \
  -e POSTGRES_PASSWORD=passwd \
  -e POSTGRES_DB=pydo \
  postgres
