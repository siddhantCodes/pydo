from flask import Flask, request, render_template, redirect
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = os.getenv(
    "DB_URI", "postgresql://postgres:passwd@localhost:5432/pydo")

db = SQLAlchemy(app)


class Todo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    todo = db.Column(db.String(), nullable=False)
    done = db.Column(db.Boolean(), default=False)

    def __repr__(self):
        return f"<Todo> {self.todo}"


db.create_all()


# all todos
@app.route("/", methods=["GET", "POST"])
def todos():
    templ = "<h1>pydo</h1><ul>"

    if request.method == "POST":
        todo = Todo(todo=request.form["todo"])
        db.session.add(todo)
        db.session.commit()
        return redirect("/")

    todos = Todo.query.all()

    return render_template("index.html", todos=todos)


@app.route("/<int:id>", methods=["DELETE"])
def delete_todo(id):
    todo = db.session.query(Todo).get(id)

    if not todo:
        return {"ok": False, "message": "todo not found"}
    db.session.delete(todo)

    db.session.commit()

    todos = Todo.query.all()

    return {"ok": True, "id": todo.id, "todo": todo.todo, "done": todo.done}


@app.route("/<int:id>", methods=["POST"])
def update_todo(id):
    t = db.session.query(Todo).get(id)

    t.todo = request.form.get("todo", t.todo)
    t.done = True if request.form.get(
        "done", "true" if t.done else "false") == "true" else False

    db.session.commit()

    return {"ok": True, "id": t.id, "todo": t.todo, "done": t.done}


if __name__ == "__main__":
    app.run(port=3000)
